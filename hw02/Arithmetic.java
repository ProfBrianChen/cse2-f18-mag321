//Mary Grabowski Homework 2
public class Arithmetic {
  //need a main method
  public static void main(String[] args) {
    //open the main method
    //Number of pairs of pants
  int numPants = 3;
//Cost per pair of pants
  double pantsPrice = 34.98;

//Number of sweatshirts
  int numShirts = 2;
//Cost per shirt
  double shirtPrice = 24.99;

//Number of belts
  int numBelts = 1;
//cost per belt
  double beltCost = 33.99;

//the tax rate
  double paSalesTax = 0.06;
    
      double totalPants = numPants*pantsPrice;
    //amount spent on pants before tax
      double totalShirts = numShirts*shirtPrice;
    //amount spent on shirts before tax
      double totalBelts = numBelts*beltCost;
    //amount spent on belts before
    System.out.println ("Total cost of pants is $"+totalPants+"."); //print total cost of pants before tax
    System.out.println ("Total cost of shirts is $"+totalShirts+"."); //print total cost of shirts before tax
    System.out.println ("Total cost of belts is $"+totalBelts+"."); //print total cost of belts before tax
    
    double salesTaxPants = totalPants*paSalesTax;
    //find out the sales tax on the pants
      double salesTaxShirts = totalShirts*paSalesTax;
   //find out the sales tax on shirts
      double salesTaxBelts = totalBelts*paSalesTax;
   //find out the sales tax on belts
    
    System.out.printf ("Sales tax on pants is $%.2f. \n", salesTaxPants); //print sales tax on pants
    System.out.printf ("Sales tax on shirts is $%.2f. \n", salesTaxShirts); //print sales tax on shirts
    System.out.printf ("Sales tax on belts is $%.2f. \n", salesTaxBelts); //print sales tax on belts
    
    double priceBeforeTax = totalPants+totalBelts+totalShirts;
   //find price of all items before tax
    
    System.out.println ("Total price of items before tax is $"+priceBeforeTax+"."); //print price of items before tax
    
    double totalTax = salesTaxBelts+salesTaxShirts+salesTaxPants;
    //find total amount of money spent on taxes
    System.out.printf ("The total tax spent on the items is $%.2f. \n", totalTax); //print amount of money spent on taxes
    
    double total = totalTax+priceBeforeTax;
    //find total amount paid
    System.out.printf ("Total amount spent including tax is $%.2f. \n", total); //print total amount paid
    
  }
}
