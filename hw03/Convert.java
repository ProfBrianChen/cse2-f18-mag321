//Mary Grabowski
//hw 3 problem 1
//9/18/2018
//cse 002

import java.util.Scanner;
public class Convert{
    			// main method needed for java programs
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );
          System.out.print("Enter the affected area in acres: ");
          double affectedArea = myScanner.nextDouble();
          System.out.print("Enter the rainfall in the affected area: ");
          double rainfallArea = myScanner.nextDouble();
         double acreInch = (double) affectedArea*rainfallArea;
          double meterConversion = .0097285581853061;
         double cubicMeter = (double) acreInch / meterConversion ;
          double cubicMile = (double) cubicMeter*.00000000023991275756433 ;
        System.out.println("There was "+cubicMile+" cubic miles");
        }
}
