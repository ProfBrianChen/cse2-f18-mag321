//Mary Grabowski
//cse 002
//hw 3 
import java.util.Scanner;
public class Pyramid{
    			// main method needed for java programs
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );
          System.out.print("The square side of the pyramid is (input length): ");
          double pyramidLength = myScanner.nextDouble();
          System.out.print("The height of the pyramid is (input height): ");
          double pyramidHeight = myScanner.nextDouble();
          double bottom = pyramidLength*pyramidLength;
          double pyramidVolume = (pyramidHeight*bottom)/3;
          System.out.printf ("Total volume is %.0f. \n", pyramidVolume);
        }
}