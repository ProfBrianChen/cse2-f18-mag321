//Mary Grabowski mag321 CSE 002-310 
public class WelcomeClass{
  //start hello world
  public static void main(String args[]){
   // prints hello world
     System.out.println("-----------------");
    // prints first line
    System.out.println("| Welcome Class |");
    //writes welcome class
     System.out.println("-----------------");
    //prints bottom of rectangle
     System.out.println("  ^  ^  ^  ^  ^  ^ ");
    //prints top characters
     System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    //prints lines that did not want to work
     System.out.println("<-M--A--G--3--2--1->");
    //prints username and year
     System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    //also lines that did not want to work
     System.out.println("  v  v  v  v  v  v  ");
    //prints bottom characters
     System.out.println("I am an Industrial and Systems Engineering student at Lehigh University. On campus I am involved in Leela: an Indian ");
    //using multiple lines so it looks good
  System.out.println("Fusion Dance Team, Bhangra Indian Dance Team, Tumbao Latin Dance Team, Star Wars Club, Society of Women Engineers");
    //multiple lines again
   System.out.println("Indian Student Association and Internal Relations Chair of the DanceFest committee");
    //autobiographic part is done
  }
}