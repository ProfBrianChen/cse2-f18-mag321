package lab07;
import java.util.Scanner;
import java.util.Random; 
public class Methods {
	public static String adjective() {
		int range = 9-0+1;
		String word1="";
		switch((int) (Math.random() * (range))) {
		case 0: word1 = "sticky";
			break;
		case 1: word1 = "moist";
			break;
		case 2: word1 = "wet";
			break;
		case 3: word1 = "hard";
			break;
		case 4: word1 = "thick";
			break;
		case 5: word1 = "dry";
			break;
		case 6: word1 = "round";
			break;
		case 7: word1 = "erect";
			break;
		case 8: word1 = "soft";
			break;
		case 9: word1 = "horny";
			break; }
		return word1;
		}
	
	public static String nounSub() {
		int range = 9-0+1;
		String word2="";
		switch((int) (Math.random() * (range))) {
		case 0: word2 = "Daddy";
			break;
		case 1: word2 = "Mom";
			break;
		case 2: word2 = "Elephant";
			break;
		case 3: word2 = "Brother-in-law";
			break;
		case 4: word2 = "dog";
			break;
		case 5: word2 = "president";
			break;
		case 6: word2 = "secretary";
			break;
		case 7: word2 = "teacher";
			break;
		case 8: word2 = "sister";
			break;
		case 9: word2 = "TA";
			break;
	}
		return word2;
		}
	public static String verbs() {
		int range = 9-0+1;
		String word3="";
		switch((int) (Math.random() * (range))) {
		case 0: word3 = "licked";
			break;
		case 1: word3 = "kicked";
			break;
		case 2: word3 = "ran";
			break;
		case 3: word3 = "saw";
			break;
		case 4: word3 = "mugged";
			break;
		case 5: word3 = "picked";
			break;
		case 6: word3 = "walked";
			break;
		case 7: word3 = "pushed";
			break;
		case 8: word3 = "moved";
			break;
		case 9: word3 = "stole";
		break;
		}
		return word3; }

		
	public static String nounObj() {
		int range = 9-0+1;
		String word4="";
		switch((int) (Math.random() * (range))) {
		case 0: word4 = "bike";
			break;
		case 1: word4 = "plant";
			break;
		case 2: word4 = "shoe";
			break;
		case 3: word4 = "umbrella";
			break;
		case 4: word4 = "phone";
			break;
		case 5: word4 = "desk";
			break;
		case 6: word4 = "computer";
			break;
		case 7: word4 = "hair";
			break;
		case 8: word4 = "exam";
			break;
		case 9: word4 = "hoodie";
			break;
	}
		return word4;
	}
	public static String sentenceGenerator() {
		String a = "The "+adjective();
		a += " "+nounSub()+" "+verbs()+ " the " + nounObj()+ ". ";
		return a;  }
	
	public static void main(String args[]) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println("How many sentences do you want?");
		int sent = myScanner.nextInt();
		String sentence="";
		for (int i = 1; i<=sent; i++) {
			sentence += sentenceGenerator();

	}
		System.out.println(sentence); }}
		