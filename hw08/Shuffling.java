
import java.util.Scanner; //import so we can import more things
import java.util.Random; //import for random choosing


public class Shuffling{ 
	public static void main(String[] args) { 
		Scanner myScanner = new Scanner(System.in); 
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //card numbers
		String[] suitNames={"C","H","S","D"};    //card suits
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int repeat = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13];
			System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards);   //need method to print
		shuffle(cards);  //call method to shuffle them bad boys
		printArray(cards); //again method to print
		while(repeat == 1){  
			hand = getHand(cards,index,numCards); 
			printArray(hand);
			index -=  numCards;
			if (numCards > index) {
				System.out.println("New Deck created");
				shuffle(cards);
				index = 51;
			}

			System.out.println("If you want another hand drawn, enter 1"); 
			repeat = myScanner.nextInt(); 
		}  
	}


	public static void printArray(String[] l) { 
		for (int i = 0; i< l.length; i++) {
			System.out.println(l[i]+" ");
		}
	}

	public static String [] getHand(String [] array, int index, int numCards) { 
		String [] hand = new String[5]; 

		for (int i = 0; i<numCards;i++) {
			hand[i] = array[index]; 
			index--;
		}
		return hand;
	}

	public static String [] shuffle(String[] array) { //method to shuffle cards
		Random x = new Random();
		for (int i = array.length - 1; i > 0; --i) { 
			int y = x.nextInt(i + 1);
			String temp = array[i]; 
			array[i] = array[y];
			array[y] = temp;
		}
		return array;
	}
}