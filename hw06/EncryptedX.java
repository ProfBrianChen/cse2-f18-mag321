
import java.util.Scanner;

public class EncryptedX {
	public static void main(String args[]) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Enter an integer between 1 and 100");
		int input = myScanner.nextInt();
		
		while(!(input>=1 && input<=100)) {
			System.out.println("Incorrect input type. Try again.");
			System.out.println("Enter an integer between 1 and 100");
			input = myScanner.nextInt();
}
		int x,y;

		for (x = 1; x <= input+1; x++ ) 
		{
			for (y=1; y<= input+1; ++y) {
				if (y==x || x==input-y+2) {
					System.out.print(" ");
				
				}
				else 
				System.out.print("*");
			}
			System.out.println();
} } }