import java.util.Scanner;



public class Hw05 {
	public static void main (String [] args) {
		Scanner myScanner = new Scanner (System.in);
    System.out.println("How many times do you want to generate hands?");
    int numGenerate = myScanner.nextInt();
    
    int i=0;
    int counter1pair = 0;
    int counter2pair =0;
    int counter3kind=0;
    int counter4kind = 0;
    int randomNum;
    int randomNum2;
    int randomNum3;
    int randomNum4;
    int randomNum5;
   
    while (i<numGenerate){
      randomNum = (int )(Math.random() * (52) + 1);
      do{
        randomNum2 = (int )(Math.random() * (52) + 1);
      }while (randomNum2==randomNum);
      do{
       randomNum3 = (int )(Math.random() * (52) + 1);
      }while (randomNum3==randomNum||randomNum3==randomNum2);
      do{
        randomNum4 = (int )(Math.random() * (52) + 1);
      }while (randomNum4==randomNum||randomNum4==randomNum2||randomNum4==randomNum3);
      do{
        randomNum5 = (int )(Math.random() * (52) + 1);
      }while(randomNum5==randomNum||randomNum5==randomNum2||randomNum5==randomNum3||randomNum5==randomNum4);
      int A= randomNum%13;
      int B=randomNum2%13;
      int C=randomNum3%13;
      int D=randomNum4%13;
      int E=randomNum5%13;
      
      
      int pairs = 0;
      if (A==B) {
        pairs++; }
      if (A==C) {
        pairs++;  }
      if (A==D) {
        pairs++; }
      if (A==E) {
        pairs++; }
      if (B==C){
        pairs++; }
      if (B==D){
        pairs++; }
      if (B==E){
        pairs++; }
      if (C==D){
        pairs++; }
      if (C==E){
        pairs++; }
      if (D==E){
        pairs++; }
       
    if (pairs==2) {
      counter2pair++;
    }
    if (pairs==6) {
      counter4kind++;
    }
    if (pairs==1 || pairs==4) {
      counter1pair++;
    }
    if (pairs==3 || pairs==4) {
      counter3kind++;
    }   
      
      
     
    i++;
    }
    System.out.println ("The number of loops is " +numGenerate);
    System.out.println("The probability of 1 pair is " +((double)counter1pair/(double)numGenerate));
    System.out.println("The probability of 2 pair is " +((double)counter2pair/(double)numGenerate));
    System.out.println("The probability of 3 of a kind is " +((double)counter3kind/(double)numGenerate));
    System.out.println("The probability of 4 of a kind is " +((double)counter4kind/(double)numGenerate));
    
  }
}
    
    