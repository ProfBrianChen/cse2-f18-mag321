//CSE 002 lab 4
//Mary Grabowski
//Card Generator
public class CardGenerator{
  //create a public class
  public static void main(String [] args){
    String suitName="";
    //create a string for suits
    String cardNumber="";
    //create a string for card number
    int randomNum = (int )(Math.random() * (52 + 1) + 1);
    //get a random number from 1 - 52
    
    if ((randomNum >= 1) && (randomNum <= 13)) {
      suitName = "Diamonds"; } //it will be diamonds
    else if ((randomNum >= 14) && (randomNum <= 26)) {
      suitName = "Clubs"; } //it will be clubs
    else if ((randomNum >= 27) && (randomNum <= 39)) {
      suitName = "Hearts"; } //it will be hearts
    else if ((randomNum >= 40) && (randomNum <= 52)) {
      suitName = "Spades"; } //it will be spades
    
    switch(randomNum) { //go into the other string which is card number
      case 1: 
      case 14:
      case 27:
      case 40: // all aces 
        System.out.println("You got an Ace of "+suitName+".");
        break;
      case 11:
      case 24:
      case 37:
      case 50: //all jacks
        System.out.println("You got a Jack of "+suitName+".");
        break;
      case 12:
      case 25:
      case 38:
      case 51: //all queens
        System.out.println ("You got a Queen of "+suitName+".");
        break;
          case 13:
      case 26:
      case 39:
      case 52: //all kings
        System.out.println ("You got a King of "+suitName+".");
        break;
      case 2:
      case 15:
      case 28:
      case 41: //all 2s
        System.out.println ("You got a 2 of "+suitName+".");
        break;
      case 3:
      case 16:
      case 29:
      case 42: //all 3s
        System.out.println ("You got a 3 of "+suitName+".");
        break;
      case 4:
      case 17:
      case 30:
      case 43: //all 4s
        System.out.println ("You got a 4 of "+suitName+".");
        break;
      case 5:
      case 18:
      case 31:
      case 44: //all 5s
        System.out.println ("You got a 5 of "+suitName+".");
        break;
      case 6:
      case 19:
      case 32:
      case 45: //all 6s
        System.out.println ("You got a 6 of "+suitName+".");
        break;
      case 7:
      case 20:
      case 33:
      case 46: //all 7s
        System.out.println("You got a 7 of "+suitName+".");
        break;
      case 8:
      case 21:
      case 34:
      case 47: //all 8s
        System.out.println("You got an 8 of "+suitName+".");
        break;
      case 9:
      case 22:
      case 35:
      case 48: //all 9s
        System.out.println("You got a 9 of "+suitName+".");
        break;
      case 10:
      case 23:
      case 36:
      case 49: //all 10s
        System.out.println("You got a 10 of "+suitName+".");
        break;
          
     //we done bois
    }   
  }
}



