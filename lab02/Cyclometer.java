//Mary Grabowski lab 2 cyclometer lab
//Cyclometer measures the distance and rotation
public class Cyclometer {
      //need a main method for all java programs
  public static void main(String[] args) {  //open the main method
  int secsTrip1=480; //seconds of first trip
  int secsTrip2=3220; //seconds of second trip
  int countsTrips1=1561; //number of rotations of first trip
  int countsTrips2=9037; //number of rotations for second trip
    
    double wheelDiameter=27.0, //diameter of the wheel
    PI=3.14159, //pi for the circumferance of the wheel
    feetPerMile=5280, //how many feet are in a mile
    inchesPerFoot=12, //how many inches in a foot
    secondsPerMinute=60; //how many seconds are in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //declaring distance of trips
    
    System.out.println("Trip 1 took "+
                       (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrips1+" counts."); //how long trip 1 took and how many counts of the wheel
    System.out.println ("Trip 2 took "+ 
                       (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrips2+" counts."); //how long trip 2 took and how many counts of the wheel
    distanceTrip1=countsTrips1*wheelDiameter*PI; //gives distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distance in miles
    distanceTrip2=countsTrips2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance in miles
    totalDistance=distanceTrip1+distanceTrip2; //gives total distance
    //print the output data
    System.out.println ("Trip 1 was "+distanceTrip1+" miles"); //prints the distance of the first trip
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints the distance of the second trip
    System.out.println("The Total distance was "+totalDistance+" miles"); //prints the total distance
    
    
    
  }
}