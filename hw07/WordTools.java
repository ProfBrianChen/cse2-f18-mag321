import java.util.Scanner;
//mary grabowski
//10/30/2018

public class WordTools { //open the main method
 public static void main (String [] args) {
		printMenu(sampleText()); //using the sample text thingy
		
		
	}

	public static String sampleText() { //entering the sample text
		Scanner input = new Scanner (System.in);
		System.out.println("Enter a string: ");
		String in = input.nextLine();
		System.out.println("You entered: "+in);
		return in;
	}

	public static void printMenu(String menu) {  //define the print menu
		Scanner input = new Scanner (System.in);

		System.out.println("Menu");
		System.out.println("1 - Number of non-whitespace characters");
		System.out.println("2 - Number of words");
		System.out.println("3 - Find text");
		System.out.println("4 - Replace all !'s");
		System.out.println("5 - Shorten spaces");
		System.out.println("6 - Quit");
		System.out.println("Choose an option: ");
		boolean test = input.hasNextInt(); 
		while (!test) {
			System.out.println("Enter a valid option of integers");
			String temp = input.next();
			test = input.hasNextInt();
		}

		int x = input.nextInt();

		switch (x) { 
		case 1: 
			numberOfNWCharacters(menu);  
			break;
		case 2:
			numberOfWords (menu);
			break;
		case 3:
			System.out.println("Enter a word or phrase to be found: ");
			String search = input.next();
			findText(search, menu);
			break;
		case 4: 

			System.out.println(replace(menu));
			break;
		case 5: 

			System.out.println("Edited text: "+ shorten(menu));
			break;
		case 6:
			System.out.println("Adios amigos");
			break;
		default:
			System.out.println("Enter valid options for input 6 to exit"); 
			printMenu(menu); 

		}

	}



	public static int numberOfNWCharacters(String line) { //method to find number of nonwhite characters
		int count = 0;
		for (int i = 0; i < line.length(); i++) {
			
			
	
			if (Character.isLetter(line.charAt(i))) 
				count++;
		}
		System.out.println("Number of non whitespace characters: "+count);
		return count;
	}



	public static int numberOfWords(String word)  {  //trying to find number of words 
		int count=0;  

		char ch[]= new char[word.length()];      
		for(int i=0;i<word.length();i++)  
		{  
			ch[i]= word.charAt(i);   
			if( ((i>0)&&(ch[i]!=' ')&&(ch[i-1]==' ')) || ((ch[0]!=' ')&&(i==0)) )   
				count++;  
		}  
		System.out.println("The number of words: "+count);
		return count;  
	}  



	public static void findText(String search, String body)  { //method to find words

		String a[] = body.split(" "); 


		int count = 0; 
		for (int i = 0; i < a.length; i++) 
		{ 

			if (search.equals(a[i])) 
				count++; 
		} 

		System.out.println("'"+search+"'"+ " occured "+ count+ " times.");
	}



	public static String replace(String replace) { //method to replace exclamation points
		String replaceString = replace.replace("!", "."); 
		return replaceString; 
	}


	public static String shorten(String str) { //method to get rid of spaces
		String shorten = "";
		if(str.indexOf("  ") >= 0) {
			shorten = str.replaceAll(" +", " ");
			return shorten;
		}
		else  {
			return str;
		}

	}


}

